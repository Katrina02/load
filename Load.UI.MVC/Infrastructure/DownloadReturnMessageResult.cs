﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Load.Infrastructure
{
    public class DownloadReturnMessageResult : ActionResult
    {
        public DownloadReturnMessageResult()
        {
        }

        public DownloadReturnMessageResult(string fileName)
        {
            this.Filename = fileName;
            //this.ReportType = reportType;

        }

        public string Filename
        {
            get;
            set;
        }


        public override void ExecuteResult(ControllerContext context)
        {
            try
            {
                if (!String.IsNullOrEmpty(this.Filename))
                {
                    //string mimeType = Unilab.Core.Libraries.Tools.MIMEAssistant.GetMIMEType(this.Filename.ToLower());
                    //HttpContext.Current.Response.AddHeader("Content-Type", mimeType);

                    context.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode(this.Filename));

                    string filePath = string.Empty;

             //       filePath = string.Format(@"{0}\{1}\{2}", Configuration.ReportExportedFolder, this.Filename);


                    context.HttpContext.Response.TransmitFile(filePath);
                }

                context.HttpContext.Response.Write("Invalid path");

            }
            catch (Exception ex)
            {
                context.HttpContext.Response.Write("Error: " + ex.Message);
            }
        }

    }
}