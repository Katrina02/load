﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Load.Infrastructure
{
    public class DownloadExcelReportResult : ActionResult
    {
        public DownloadExcelReportResult()
        {
        }

        public DownloadExcelReportResult(string fileName)
        {
            this.Filename = fileName;
            //this.ReportType = reportType;

        }

        //public string ReportType
        //{
        //    get;
        //    set;
        //}

        public string Filename
        {
            get;
            set;
        }



        public override void ExecuteResult(ControllerContext context)
        {
            try
            {
                if (!String.IsNullOrEmpty(this.Filename))
                {
                    //string mimeType = Unilab.Core.Libraries.Tools.MIMEAssistant.GetMIMEType(this.Filename.ToLower());
                    //HttpContext.Current.Response.AddHeader("Content-Type", mimeType);

                    context.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode(this.Filename));

                    string filePath = string.Empty;

                    filePath = @"C:\Users\USER\Documents\Visual Studio 2015\Projects\Load\Load.UI.MVC\Templates\TEHNICIANS Template_ETFRS Visayas.xlsx";


                    context.HttpContext.Response.TransmitFile(filePath);
                }

                context.HttpContext.Response.Write("Invalid path");

            }
            catch (Exception ex)
            {
                context.HttpContext.Response.Write("Error: " + ex.Message);
            }
        }

    }
}