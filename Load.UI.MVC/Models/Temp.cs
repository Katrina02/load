﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Load.Models
{
    public class Temp
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}