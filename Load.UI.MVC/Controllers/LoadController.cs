﻿using Load.Infrastructure;
using Load.Service.Abstract;
using Load.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Load.Controllers
{
    public class LoadController : Controller
    {
        //#region MEMBERS
        //    IExcelPackageFormatService _excelPackageFormatService;
        //#endregion

        //#region CONSTRUCTOR
        //public LoadController(IExcelPackageFormatService excelPackageFormatService)
        //{
        //    _excelPackageFormatService = excelPackageFormatService;
        //}
        //#endregion
        // GET: Load
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        //public ActionResult Interop()
        //{
        //    // Create two DataTable instances.
        //    DataTable table1 = new DataTable("KathInfo");
        //    table1.Columns.Add("name");
        //    table1.Columns.Add("id");


        //    // Create a DataSet and put both tables in it.
        //    DataSet ds = new DataSet("office");
        //    ds.Tables.Add(table1);


        //    DataSet dataSet = ds;

        //    //Creating Object of Microsoft.Office.Interop.Excel and creating a Workbook
        //    var excelApp = new Application();
        //    excelApp.Visible = true;
        //    excelApp.Workbooks.Add();

        //    Worksheet workSheet = (Worksheet)excelApp.ActiveSheet; //creating excel worksheet
        //    workSheet.Name = "Sale"; //name of excel file

        //    //LINQ to get Column of dataset table
        //    var columnName = dataSet.Tables[0].Columns.Cast<DataColumn>()
        //                         .Select(x => x.ColumnName)
        //                         .ToArray();
        //    int i = 0;
        //    //Adding column name to worksheet
        //    foreach (var col in columnName)
        //    {
        //        i++;
        //        workSheet.Cells[1, i] = col;
        //    }

        //    //Adding records to worksheet
        //    int j;
        //    for (i = 0; i < dataSet.Tables[0].Rows.Count; i++)
        //    {
        //        for (j = 0; j < dataSet.Tables[0].Columns.Count; j++)
        //        {
        //            workSheet.Cells[i + 2, j + 1] = Convert.ToString(dataSet.Tables[0].Rows[i][j]);
        //        }
        //    }

        //    //Saving the excel file to “e” directory
        //    workSheet.SaveAs("e:\\" + workSheet.Name);
        //    return RedirectToAction("Index");
        //}
        // [HttpPost]
        //public ActionResult Upload(int chuck = 0, string name = "")
        //{
        //    try
        //    {
        //        HttpContextBase httpContext = this.ControllerContext.HttpContext;
        //        if (!string.IsNullOrWhiteSpace(name) &&
        //            httpContext.Request.Files != null &&
        //            httpContext.Request.Files.Count > 0)
        //        {
        //            HttpPostedFileBase fileUpload = httpContext.Request.Files[0];
        //            if (fileUpload.InputStream.Length > 0)
        //            {
        //                long chunkSize = fileUpload.InputStream.Length;
        //                long totalFileSize = 0L;
        //                string filename = Path.Combine(Utilities.GetUploadTempFolder(), name);
        //                if (chuck == 0)
        //                {
        //                    if (System.IO.File.Exists(filename))
        //                    {
        //                        System.IO.File.Delete(filename);
        //                    }
        //                }

        //                using (FileStream fs = new FileStream(filename, FileMode.Append))
        //                {
        //                    fileUpload.InputStream.CopyTo(fs);
        //                    totalFileSize = fs.Length;
        //                }

        //                return Json(new { ok = true, chuckSize = chunkSize, totalFileSize = totalFileSize, message = "ok" });
        //            }
        //        }

        //        return Json(new { ok = false, message = "Invalid Parameters." });
        //    }

        //    catch (Exception ex)
        //    {
        //        return Json(new { ok = false, message = ex.Message });
        //    }
        //}

        public ActionResult DownloadExcel(string file)
        {
            return new DownloadExcelReportResult { Filename = file };
        }

        //[HttpGet]
        //public JsonResult CheckFileParameter(string tempFilename)
        //{
        //    try
        //    {
        //        //if (!_importReportingService.CheckIfFileExistsInTempFolder(tempFilename))
        //        //{
        //        //    return Json(new { ok = false, message = "Please provide attachment." }, JsonRequestBehavior.AllowGet);
        //        //}

        //        var extension = System.IO.Path.GetExtension(tempFilename);

        //        if (!(extension.ToLower() == ".xlsx" || extension.ToLower() == ".xls"))
        //        {
        //            return Json(new { ok = false, message = "File extension incorrect" }, JsonRequestBehavior.AllowGet);
        //        }

        //        //if (_capexDetailViewModelService.CheckParameter(tempFilename))
        //        //{
        //        //    return Json(new { ok = true, message = "The Record already exist, do you want to overwrite it?" }, JsonRequestBehavior.AllowGet);
        //        //}

        //        return Json(new { ok = true, message = "New" }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ex.Message;

        //        if (errorMessage == "Object reference not set to an instance of an object.")
        //        {
        //            errorMessage = "Please upload correct template";
        //        }

        //        return Json(new { ok = false, message = errorMessage }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //public ActionResult SaveFileUpload(string tempFilename, string fileName)
        //{
        //    try
        //    {
        //        //if (!_importReportingService.CheckIfFileExistsInTempFolder(tempFilename))
        //        //{
        //        //    return Json(new { ok = false, message = "Please provide attachment." }, JsonRequestBehavior.AllowGet);
        //        //}

        //        var extension = System.IO.Path.GetExtension(tempFilename);

        //        if (!(extension.ToLower() == ".xlsx" || extension.ToLower() == ".xls"))
        //        {
        //            return Json(new { ok = false, message = "File extension incorrect" }, JsonRequestBehavior.AllowGet);
        //        }

        //        string returnFileName = string.Empty;
        //        ResultType<string> result = _excelPackageFormatService.CreateReportUpload(fileName, tempFilename, out returnFileName);

        //        if (result.Success)
        //        {
        //            return Json(new { ok = true, message = result.ErrorMessage, result.Result }, JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            return Json(new { ok = false, message = result.ErrorMessage, returnFileName = returnFileName }, JsonRequestBehavior.AllowGet);
        //        }

        //        return Json(new { ok = false, message = result.ErrorMessage }, JsonRequestBehavior.AllowGet);
        //    }

        //    catch (Exception ex)
        //    {
        //        if (ex.Message == "External table is not in the expected format.")
        //        {
        //            return Json(new { ok = false, message = "Failed to upload. Template is empty." }, JsonRequestBehavior.AllowGet);
        //        }

        //        var type = ex.GetType();
        //        if (type != null && type.FullName == "System.Data.OleDb.OleDbException")
        //        {
        //            return Json(new { ok = false, message = "Please use standard template. " }, JsonRequestBehavior.AllowGet);
        //        }

        //        return Json(new { ok = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //public ActionResult DownloadResult(string file)
        //{
        //    return new DownloadReturnMessageResult { Filename = file };
        //}
    }
}