﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Load.Controllers
{
    public class LoadUIController : Controller
    {
        string path = System.Configuration.ConfigurationManager.AppSettings["ExcelPath"];
        string outputpath = System.Configuration.ConfigurationManager.AppSettings["ExcelOut"];

        // GET: LoadUI
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        #region
        //public ActionResult ExportTech()
        //{
        //    //Modify filename
        //    var dirName = Path.GetDirectoryName(path);
        //    var FileName = Path.GetFileNameWithoutExtension(path);
        //    var extName = Path.GetExtension(path);

        //    var newname = FileName + "_" + DateTime.Now.ToString("ddMMyyyy") + extName;
        //    newname = dirName + newname;

        //    var retVal = new Load.Core.ExcelSerialize().CreateTechncians(null, newname, "Sheet1", null);
        //    var retVal1 = new Load.Core.ExcelSerialize().LoadExistingExcel(null, path, "Sheet1");
        //    var retVal2 = new Load.Core.ExcelSerialize().DownloadExcelTemplate(path, outputpath, "Sheet1");

        //    return Json(new { ok = true, data = Path.GetFileName(retVal), message = "ok" });

        //}

        //public ActionResult ExportDis()
        //{
        //    //Modify filename
        //    var dirName = Path.GetDirectoryName(path);
        //    var FileName = Path.GetFileNameWithoutExtension(path);
        //    var extName = Path.GetExtension(path);

        //    var newname = FileName + "_" + DateTime.Now.ToString("ddMMyyyy") + extName;
        //    newname = dirName + newname;


        //    var retVal = new Load.Core.ExcelSerialize().CreateDispatchers(null, newname, "Sheet1", null);
        //    var retVal1 = new Load.Core.ExcelSerialize().LoadExistingExcel(null, path, "Sheet1");
        //    var retVal2 = new Load.Core.ExcelSerialize().DownloadExcelTemplate(path, outputpath, "Sheet1");

        //    return Json(new { ok = true, data = Path.GetFileName(retVal), message = "ok" });

        //}

        #endregion

        public ActionResult ExportView(int param = 0)
        {
            //Modify filename
            var dirName = Path.GetDirectoryName(path);
            var FileName = Path.GetFileNameWithoutExtension(path);
            var extName = Path.GetExtension(path);

            var newname = "";

            var retVal = "";

            switch (param)
            {
                case 1:
                    newname = dirName + "Techncians" + DateTime.Now.ToString("ddMMyyyy") + extName;
                    retVal = new Load.Core.ExcelSerialize().CreateTechncians(null, newname, "Sheet1", null);
                    break;
                case 2:
                    newname = dirName + "Dispatchers" + DateTime.Now.ToString("ddMMyyyy") + extName;
                    retVal = new Load.Core.ExcelSerialize().CreateDispatchers(null, newname, "Sheet1", null);
                    break;
                case 3:
                    newname = dirName + "Buckets" + DateTime.Now.ToString("ddMMyyyy") + extName;
                    retVal = new Load.Core.ExcelSerialize().CreateBuckets(null, newname, "Sheet1", null);
                    break;
            }

            return Json(new { ok = true, data = Path.GetFileName(retVal), message = "ok" });
        }
        [HttpPost]
        public string UploadFiles(string value)
        {
            string uniquname = "";
            string returnMessage = "";
            // Checking no of files injected in Request object  

            //  Get all files from Request object  
            HttpFileCollectionBase files = Request.Files;
            for (int i = 0; i < files.Count; i++)
            {

                HttpPostedFileBase file = files[i];
                string fname;


                // Checking for Internet Explorer  
                if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                {
                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                    fname = testfiles[testfiles.Length - 1];
                }
                else
                {
                    HttpPostedFileBase myfile = Request.Files[0];

                    if (file.ContentLength > 0)
                    {

                        if (Path.GetExtension(file.FileName).ToLower() != ".pdf")
                        {
                            returnMessage = "Invalid file type. Only PDF files are accepted.";
                        }
                        else
                        {
                            // extract only the filename
                            var fileName = Path.GetFileName(file.FileName);//10:42:54 AM
                            var fileNoExtension = fileName.ToLower().Replace(".pdf", "");

                            uniquname = DateTime.Now.ToString("yyyymmdd") + DateTime.Now.ToLongTimeString().ToString().Replace(":", "");
                            uniquname = uniquname.Replace(" ", "");
                            uniquname = uniquname.Replace("PM", "");
                            uniquname = uniquname.Replace("AM", "");
                            uniquname = "TECHICIANS Template_ETFRS" + uniquname + "_" + fileNoExtension + ".pdf";

                            var path = Path.GetTempPath();
                                //(Trac.Core.Common.Configuration.UploadRootFolder, Trac.Core.Common.Configuration.UploadTempFolder);
                            var filename = path + uniquname;


                            System.Web.HttpContext.Current.Session["pdffullPathName"] = filename;
                            System.Web.HttpContext.Current.Session["pdfFileName"] = uniquname;


                            file.SaveAs(filename);

                        }
                    }

                }
            }
            return returnMessage;
        }

    }
}