﻿//block ui
function block(id, message) {
    if (message == null) {
        message = "Processing...";
    }

    if (id === null) {
        $.blockUI({
            //message: '<div class="blockUI-message">' + message + '</div>',
            message: '<img src="Content/images/etrac-loader.gif" alt="">',
            css: { border: 'none', width: 'auto' },
            overlayCSS: { backgroundColor: '#C0C0C0' }
        });
    }
    else {
        $('#' + id).block({
            //message: '<div class="blockUI-message">' + message + '</div>',
            message: '<img src="Content/images/etrac-loader.gif" alt="">',
            css: { border: 'none', width: 'auto' },
            overlayCSS: { backgroundColor: '#C0C0C0' }
        });
    }
}

function unblock(id) {
    if (id === null) {
        $.unblockUI();
    }
    else {
        $('#' + id).unblock();
    }
}

function blockPage(message) {
    block(null, message);
}

function unblockPage() {
    unblock(null);
}

function executeAction(divId, targetUrl, paramData, callback) {
    // block(divId);
    blockPage();
    $.ajax({
        type: "POST",
        url: targetUrl,
        data: paramData,
        success: function (newHtml) {
            setTimeout(function () {
                $("#" + divId).html(newHtml);
                unblockPage();
                // unblock(divId);
                //execute callback if there's any
                if (callback != undefined) {
                    callback();
                }

            }, 500);
        },
        error: function (request, textStatus, errorThrown) {
            bootbox.alert("AJAX error: " + request.statusText);
            //unblock(divId);
            unblockPage();
        }
    });
}


function requestAction(requestType, targetUrl, paramData, divId , callback) {

    bootbox.confirm("Are you sure you want save?", function (result) {
        if (result === false) {
            return close;
            return false;
        }
        else {

            block(divId);
            $.ajax({
                type: requestType,
                url: targetUrl,
                data: paramData,
                success: function (result) {
                    setTimeout(function () {
                        //bootbox.alert(result.message);
                        bootbox.confirm(result.message, function (result) {
                            if (callback != undefined) {
                                callback();
                            }
                            //                            backToList();
                        });

                        unblock(divId);
                    }, 500);
                },
                error: function (request, textStatus, errorThrown) {
                    bootbox.alert("AJAX error: " + request.statusText);
                    unblock(divId);
                }
            });
        }
    });
}


function requestAction2(message ,requestType, targetUrl, paramData, divId, callback) {

    bootbox.confirm(message , function (result) {
        if (result === false) {
            return close;
            return false;
        }
        else {

            block(divId);
            $.ajax({
                type: requestType,
                url: targetUrl,
                data: paramData,
                success: function (result) {
                    setTimeout(function () {
                        //bootbox.alert(result.message);
                        bootbox.confirm(result.message, function (result) {
                            if (callback != undefined) {
                                callback();
                            }
                            //                            backToList();
                        });

                        unblock(divId);
                    }, 500);
                },
                error: function (request, textStatus, errorThrown) {
                    bootbox.alert("AJAX error: " + request.statusText);
                    unblock(divId);
                }
            });
        }
    });
}


function requestSaveReturns(requestType, targetUrl, paramData, divId, obj) {

    bootbox.confirm("Are you sure you want save?", function (result) {
        if (result === false) {
            return close;
            return false;
        }
        else {

            block(divId);
            $.ajax({
                type: requestType,
                url: targetUrl,
                data: paramData,
                success: function (result) {
                    setTimeout(function () {
                        //bootbox.alert(result.message);
                        bootbox.confirm(result.message, function (result2) {
                            $('#ReturnedQuantity').val(result.returnedQty);
                            $('#RemainingQuantity').val(result.remainingQty);
                            $('#ReturnNewQuantity').val('');
                            
                        });

                        unblock(divId);
                    }, 500);
                },
                error: function (request, textStatus, errorThrown) {
                    bootbox.alert("AJAX error: " + request.statusText);
                    unblock(divId);
                }
            });
        }
    });
}



function RedirectTo(targetURL) {
    window.location.href = targetURL;
}

function ClickFor(targetControlId) {
    $("#" + targetControlId).click();
}