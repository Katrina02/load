// Detect IE10 & IE11

if (navigator.userAgent.match('MSIE 10.0;') || navigator.userAgent.match('Trident/7.0;')) {
    $('.nav_menu').css('padding', '9px 10px');
    $('.toggle').css('margin-top', '9px');
    $('.module__name').css('padding', '9px 0');
    $('.graph, .tile').css('zoom','100%');

    if (!('remove' in Element.prototype)) {
        Element.prototype.remove = function () {
            if (this.parentNode) {
                this.parentNode.removeChild(this);
            }
        };
    }
};



//Preloader

function preloadPage() {
    $(".pageloader").fadeOut(800).delay(100).queue(function () {
        $(this).remove().dequeue();
    });;
}

//Sidebar Icon Toggle

function sbIconToggle() {
    var dbg = "body .container.body .right_col--home",
        logoIcon = document.getElementById('logoIcon'),
        navsmMenu = '.nav.side-menu li a';
    //Fix for text ellipsis IE issue
    $(navsmMenu).delay(100).queue(function () {
        $(this).addClass("w100").dequeue();
    });;

    var windowWidth = $(window).width();
    if (windowWidth <= 599) {
        $("#menu_toggle").on("click", function () {
            if ($("body").hasClass("nav-sm")) {
                $("#menu_toggle-icon").removeClass("fa-angle-double-right").addClass("fa-angle-double-left");
                $(dbg).removeClass('right_col--md');
                $(navsmMenu).addClass('w100');
            } else {
                $("#menu_toggle-icon").removeClass("fa-angle-double-left").addClass("fa-angle-double-right");
                $(dbg).addClass('right_col--md');
                $(navsmMenu).removeClass('w100');
            }
        });
    } else {
        $("#menu_toggle").on("click", function () {
            if ($("body").hasClass("nav-sm")) {
                $("#menu_toggle-icon").removeClass("fa-angle-double-left").addClass("fa-angle-double-right");
                $(dbg).removeClass('right_col--md');
                logoIcon.src = 'assets/img/logo1-icon.png';
                $(navsmMenu).addClass('w100');
            } else {
                $("#menu_toggle-icon").removeClass("fa-angle-double-right").addClass("fa-angle-double-left");
                $(dbg).addClass('right_col--md');
                logoIcon.src = 'assets/img/logo1-icon-blank.png';
                $(navsmMenu).removeClass('w100');
            }
        });
    }

}

//Collapsed Sidebar Set to Default

function menuToggle() {
    if ($('body').hasClass('nav-md')) {
        $("#sidebar-menu").find('li.active ul').hide();
        $("#sidebar-menu").find('li.active').addClass('active-sm').removeClass('active');
    } else {
        $("#sidebar-menu").find('li.active-sm ul').show();
        $("#sidebar-menu").find('li.active-sm').addClass('active').removeClass('active-sm');
    }
    $('body').toggleClass('nav-md nav-sm');
    setHeight();
}

function setHeight() {
    $('.right_col').css('min-height', $(window).height());
    var bodyHeight = $('body').outerHeight();
    var footerHeight = $('body').hasClass('footer_fixed') ? -10 : $('footer').height();
    var leftColHeight = $('.left_col').eq(1).height() + $('.sidebar-footer').height();
    var contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;
    contentHeight -= $('.nav_menu').height() + footerHeight;
    $('.right_col').css('min-height', contentHeight);
}


//Filters

function showAddFilter() { 
    $(".additional-filters").toggleClass("is-hidden");
    $('#showFilter').toggleClass('is-hidden');
    $('#hideFilter').toggleClass('is-hidden');
}




//Panels Default Collapsed 

function panelCollapsed() {
    $('.collapse-link--default').trigger("click");
}

//Fix for Right Col Height
function rightColHeight() {
    $('.right_col').delay(20).queue(function () {
        $(this).css('min-height', '100vh').dequeue();
    });;
}

$(window).load(function () {
    preloadPage(), panelCollapsed()
});

//$(document).ready(function () {
//     showFilterOption(), addFilter()
//});

$(window).on("load resize", function () {
    sbIconToggle(), rightColHeight();
    var windowWidth = $(window).width();
    if (windowWidth <= 599) {
        $('body').removeClass('nav-sm').addClass('nav-md');
    } else if (windowWidth >= 600) {
        menuToggle()
    }
});