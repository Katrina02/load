﻿
var idleTime = 0;

$(document).ready(function () {
//$(window).load(function () {
    var idleInterval = setInterval(timerIncrement, 1000); 
    $(this).mousemove(function (e) {
        idleTime = 0;
    });
    $(this).keypress(function (e) {
        idleTime = 0;
    });
});

function timerIncrement() {
    idleTime = idleTime + 1;
    if (idleTime > 3540) {
        $('#modalSession').modal('show');
        idleTime = 0;
    }
}

$('#modalSession').on('click', '#btnOk', function (e) {
    var $modalDiv = $(e.delegateTarget);
    //alert("test");
    $("#logoutForm").submit();
    $('#modalSession').modal("hide");
});
