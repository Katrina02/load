﻿//validations
function ValidateText(controlId, description) {
    var value = $.trim($('#' + controlId).val());
    $('#' + controlId).val(value);
    if (value.length < 1) {
        $('#' + controlId).focus();
        bootbox.alert('Please specify ' + description + '.', function () { bootbox.hideAll() });
        return false;
    }

    return true;
}

function ValidateDate(controlId, description) {
    var value = $.trim($('#' + controlId).val());
    $('#' + controlId).val(value);

    if (value.length < 1) {
        $('#' + controlId).focus();
        bootbox.alert('Please specify ' + description + '.', function () { bootbox.hideAll() });
        return false;
    }

    var date = Date.parse(value);

    if (date == null || isNaN(date)) {
        bootbox.alert('Invalid ' + description + ".", function () { bootbox.hideAll() });
        return false;
    }

    return true;
}

function ValidateDateRange(controlIdStart, controlIdEnd, startDescription, endDescription) {

    var startDateValue = $.trim($('#' + controlIdStart).val());
    $('#' + controlIdStart).val(startDateValue);
    var endDateValue = $.trim($('#' + controlIdEnd).val());
    $('#' + controlIdEnd).val(endDateValue);

    var startDate = Date.parse(startDateValue);
    var endDate = Date.parse(endDateValue);

    if (startDate == null || isNaN(startDate)) {
        $('#' + controlIdStart).focus();
        bootbox.alert('Invalid ' + startDescription + '.', function () { bootbox.hideAll() });
        return false;
    }

    if (endDate == null || isNaN(endDate)) {
        $('#' + controlIdEnd).focus();
        bootbox.alert('Invalid ' + endDescription + '.', function () { bootbox.hideAll() });
        return false;
    }

    if (startDate > endDate) {
        $('#' + controlIdEnd).focus();
        bootbox.alert(startDescription + ' cannot be later than ' + endDescription + '.', function () { bootbox.hideAll() });
        return false;
    }

    return true;
}

function ValidateInteger(controlId, description, minValue, maxValue) {
    var value = $.trim($('#' + controlId).val());
    var numValue;

    $('#' + controlId).val(value);
    if (value.length < 1) {
        $('#' + controlId).focus();
        bootbox.alert('Please specify ' + description + '.', function () { bootbox.hideAll() });
        return false;
    }

    if (!(!isNaN(parseInt(value, 10)) && (parseFloat(value, 10) == parseInt(value, 10)))) {
        $('#' + controlId).focus();
        bootbox.alert('Please specify a valid integer value for ' + description + '.', function () { bootbox.hideAll() });
        return false;
    }

    numValue = parseInt(value, 10)

    if (minValue != null && numValue <= minValue) {
        $('#' + controlId).focus();
        bootbox.alert('Please specify a valid integer value for ' + description + ' that is greater than ' + minValue + '.', function () { bootbox.hideAll() });
        return false;
    }

    if (maxValue != null && numValue >= minValue) {
        $('#' + controlId).focus();
        bootbox.alert('Please specify a valid integer value for ' + description + ' that is less than ' + maxValue + '.', function () { bootbox.hideAll() });
        return false;
    }

    $('#' + controlId).val(numValue);
    return true;
}

function ValidateNumber(controlId, description, minValue, maxValue) {
    var value = $.trim($('#' + controlId).val().replace(/,/g, ""));
    var numValue;

    $('#' + controlId).val(value);
    if (value.length < 1) {
        $('#' + controlId).focus();
        bootbox.alert('Please specify ' + description + '.', function () { bootbox.hideAll() });
        return false;
    }
    if (isNaN(value)) {
        $('#' + controlId).focus();
        bootbox.alert('Please specify a valid numeric value for ' + description + '.', function () { bootbox.hideAll() });
        return false;
    }

    numValue = parseFloat(value);

    if (minValue != null && numValue <= minValue) {
        $('#' + controlId).focus();
        bootbox.alert('Please specify a valid numeric value for ' + description + ' that is greater than ' + minValue + '.', function () { bootbox.hideAll() });
        return false;
    }

    if (maxValue != null && numValue >= minValue) {
        $('#' + controlId).focus();
        bootbox.alert('Please specify a valid numeric value for ' + description + ' that is less than ' + maxValue + '.', function () { bootbox.hideAll() });
        return false;
    }

    $('#' + controlId).val(numValue);

    return true;
}

//block ui
function block(id, message) {
    if (message == null) {
        message = "Processing...";
    }

    if (id === null) {
        $.blockUI({
            message: '<div class="blockUI-message">' + message + '</div>',
            css: { border: 'none', width: 'auto' },
            overlayCSS: { backgroundColor: '#C0C0C0' }
        });
    }
    else {
        $('#' + id).block({
            message: '<div class="blockUI-message">' + message + '</div>',
            css: { border: 'none', width: 'auto' },
            overlayCSS: { backgroundColor: '#C0C0C0' }
        });
    }
}

function unblock(id) {
    if (id === null) {
        $.unblockUI();
    }
    else {
        $('#' + id).unblock();
    }
}

function blockPage(message) {
    block(null, message);
}

function unblockPage() {
    unblock(null);
}

function executeAction(divId, targetUrl, paramData, callback) {
    block(divId);
    $.ajax({
        type: "POST",
        url: targetUrl,
        data: paramData,
        success: function (newHtml) {
            setTimeout(function () {
                $("#" + divId).html(newHtml);
                unblock(divId);
                //execute callback if there's any
                if (typeof (callback) == "function") {
                    callback();
                }

            }, 500);
        },
        error: function (request, textStatus, errorThrown) {
            bootbox.alert("AJAX error: " + request.statusText);
            unblock(divId);
        }
    });
}

function RedirectTo(targetURL) {
    window.location.href = targetURL;
}
goToPage