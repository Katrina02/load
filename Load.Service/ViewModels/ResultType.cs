﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Load.Service.ViewModels
{
    public class ResultType<T>
    {
        public ResultType(T result, bool success = true, string errorMessage = "")
        {
            this.Result = result;
            this.Success = success;
            this.ErrorMessage = errorMessage;
        }

        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public T Result { get; set; }
    }
}
