﻿using Load.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Load.Service.Abstract
{
    public interface IExcelPackageFormatService
    {
        ResultType<string> CreateReportUpload(string filename, string tempFilename, out string returnFileName);
    }
}
