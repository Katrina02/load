﻿
using Load.Domain.Entities;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace Load.Core
{
    public class ExcelSerialize
    {
        string retVal = "No Record.";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data">Data collection from database or any...</param>
        /// <param name="excelPath">Load excel from this Location</param>
        /// <param name="sheetName">Assign excel sheet name</param>
        /// <param name="excelProperties">You can define the excel properties[0]=Author; [1]=Title,[2]=Subject</param>
        /// <returns></returns>
        public string CreateTechncians(List<Temp> data, string excelPath, string sheetName, string[] excelProperties)
        {
            try
            {
                //List<Temp>
                //the Temp => Class name
               // if (data == null) return;


                //Create a new ExcelPackage
                using (ExcelPackage excelPackage = new ExcelPackage())
                {
                    if (excelProperties != null)
                    {
                        //Set some properties of the Excel document
                        excelPackage.Workbook.Properties.Author = excelProperties[0].ToString();
                        excelPackage.Workbook.Properties.Title = excelProperties[1].ToString();
                        excelPackage.Workbook.Properties.Subject = excelProperties[2].ToString();
                        excelPackage.Workbook.Properties.Created = DateTime.Now; 
                    }

                    //Create the WorkSheet
                    ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add(sheetName);

                    //Add some text to cell column
                    worksheet.Cells["A1"].Value = "EXTERNAL_ID/TECH_ID";
                    worksheet.Cells["B1"].Value = "RESOURCE TYPE";
                    worksheet.Cells["C1"].Value = "RESOURCE NAME";
                    worksheet.Cells["D1"].Value = "PARENT EXTERNAL ID - RESOURCE BUCKET";
                    worksheet.Cells["E1"].Value = "EMAIL ADDRESS";
                    worksheet.Cells["F1"].Value = "MOBILE/PHONE NUMBER";
                    worksheet.Cells["G1"].Value = "LOGIN";
                    worksheet.Cells["H1"].Value = "USERNAME";
                    worksheet.Cells["I1"].Value = "POSITION";
                    worksheet.Cells["J1"].Value = "FIRST NAME";
                    worksheet.Cells["K1"].Value = "MIDDLE NAME";
                    worksheet.Cells["L1"].Value = "LAST NAME";
                    worksheet.Cells["M1"].Value = "SAWA";
                    worksheet.Cells["N1"].Value = "Work Skills";
                    worksheet.Cells["O1"].Value = "Work Skills Ratio";
                    worksheet.Cells["P1"].Value = "Collaboration Group";

                    //Loop data collection here to in excel
                    //You could also use [line, column] notation:
                    //worksheet.Cells[2, 1].Value = "This is cell B1!";

                    //Save your file
                    FileInfo fi = new FileInfo(excelPath);
                    excelPackage.SaveAs(fi);
                }
                retVal = excelPath;
            }
            catch (Exception ex)
            {
                retVal = ex.Message;
                throw ex;
            }

            return retVal;
        }

        public string CreateDispatchers(List<Temp> data, string excelPath, string sheetName, string[] excelProperties)
        {
            try
            {
                //List<Temp>
                //the Temp => Class name
                // if (data == null) return;


                //Create a new ExcelPackage
                using (ExcelPackage excelPackage = new ExcelPackage())
                {
                    if (excelProperties != null)
                    {
                        //Set some properties of the Excel document
                        excelPackage.Workbook.Properties.Author = excelProperties[0].ToString();
                        excelPackage.Workbook.Properties.Title = excelProperties[1].ToString();
                        excelPackage.Workbook.Properties.Subject = excelProperties[2].ToString();
                        excelPackage.Workbook.Properties.Created = DateTime.Now;
                    }

                    //Create the WorkSheet
                    ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add(sheetName);

                    //Add some text to cell column
                    worksheet.Cells["A1"].Value = "LOGIN";
                    worksheet.Cells["B1"].Value = "USERNAME";
                    worksheet.Cells["C1"].Value = "USER_TYPE";
                    worksheet.Cells["D1"].Value = "POSITION";
                    worksheet.Cells["E1"].Value = "FIRST NAME";
                    worksheet.Cells["F1"].Value = "MIDDLE NAME";
                    worksheet.Cells["G1"].Value = "LAST NAME";
                    worksheet.Cells["H1"].Value = "SAWA";
                    worksheet.Cells["I1"].Value = "RESOURCE";
                    worksheet.Cells["J1"].Value = "Collaboration Group";

                    //Loop data collection here to in excel
                    //You could also use [line, column] notation:
                    //worksheet.Cells[2, 1].Value = "This is cell B1!";

                    //Save your file
                    FileInfo fi = new FileInfo(excelPath);
                    excelPackage.SaveAs(fi);
                }
                retVal = excelPath;
            }
            catch (Exception ex)
            {
                retVal = ex.Message;
                throw ex;
            }

            return retVal;
        }

        public string CreateBuckets(List<Temp> data, string excelPath, string sheetName, string[] excelProperties)
        {
            try
            {
                //List<Temp>
                //the Temp => Class name
                // if (data == null) return;


                //Create a new ExcelPackage
                using (ExcelPackage excelPackage = new ExcelPackage())
                {
                    if (excelProperties != null)
                    {
                        //Set some properties of the Excel document
                        excelPackage.Workbook.Properties.Author = excelProperties[0].ToString();
                        excelPackage.Workbook.Properties.Title = excelProperties[1].ToString();
                        excelPackage.Workbook.Properties.Subject = excelProperties[2].ToString();
                        excelPackage.Workbook.Properties.Created = DateTime.Now;
                    }

                    //Create the WorkSheet
                    ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add(sheetName);

                    //Add some text to cell column
                    worksheet.Cells["A1"].Value = "EXTERNAL ID";
                    worksheet.Cells["B1"].Value = "RESOURCE NAME";
                    worksheet.Cells["C1"].Value = "PARENT EXTERNAL ID";

                    //Loop data collection here to in excel
                    //You could also use [line, column] notation:
                    //worksheet.Cells[2, 1].Value = "This is cell B1!";

                    //Save your file
                    FileInfo fi = new FileInfo(excelPath);
                    excelPackage.SaveAs(fi);
                }
                retVal = excelPath;
            }
            catch (Exception ex)
            {
                retVal = ex.Message;
                throw ex;
            }

            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data">Data collection from database or any...</param>
        /// <param name="excelPath">Load excel from this Location</param>
        /// <param name="sheetName">Assign excel sheet name</param>
        /// <returns></returns>
        public string LoadExistingExcel(List<Temp> data, string excelPath, string sheetName)
        {
            try
            {
                //if (data == null) return retVal;
                //create a list to hold all the values
                List<string> excelData = new List<string>();

               
                //or if you use asp.net, get the relative path
                byte[] bin = File.ReadAllBytes(excelPath);

                //create a new Excel package in a memorystream
                using (MemoryStream stream = new MemoryStream(bin))
                using (ExcelPackage excelPackage = new ExcelPackage(stream))
                {
                    //loop all worksheets
                    foreach (ExcelWorksheet worksheet in excelPackage.Workbook.Worksheets)
                    {
                        //filter spicific sheetname
                        //add if condition here
                        if (worksheet.Name == sheetName)
                        {

                            //Get Only the header name
                            foreach (var firstRowCell in worksheet.Cells[worksheet.Dimension.Start.Row, worksheet.Dimension.Start.Column, 1, worksheet.Dimension.End.Column])
                            {
                                excelData.Add(firstRowCell.Text);
                            }

                            //To get the Data in an excel
                            Temp test;
                            List<Temp> testCollection = new List<Temp>();

                            for (int i = 2; i <= worksheet.Dimension.End.Row; i++)
                            {
                                //for (int j = worksheet.Dimension.Start.Column; j <= worksheet.Dimension.End.Column; j++)
                                //{
                                //    test = new Temp();
                                //    test.Name = worksheet.Cells[i, 3].Value.ToString(); //3 fro resource person
                                //    test.Phone = worksheet.Cells[i, 6].Value.ToString(); //3 fro phone number
                                //    test.Email = worksheet.Cells[i, 5].Value.ToString(); //3 fro email address
                                //    testCollection.Add(test);
                                //    break;
                                //}
                            }

                        }
                    }
                }

                retVal = excelPath;
            }
            catch (Exception ex)
            {
                retVal = ex.Message;
                throw ex;
            }

            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="templatePath">Location of the Excel template(Source)</param>
        /// <param name="outputpath">Destination of the excel</param>
        /// <param name="sheetName">Assign excel sheet name</param>
        /// <returns></returns>
        public string DownloadExcelTemplate(string templatePath, string outputpath, string sheetName)
        {
            try
            {
                FileInfo template = new FileInfo(templatePath);
                //using (var package = new ExcelPackage(template))
                //{

                using (ExcelPackage excelPackage = new ExcelPackage(template))
                {
                    var workbook = excelPackage.Workbook;
                    //Get the First Sheet
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[sheetName];
                    // var worksheet = workbook.Worksheets.First();

                    excelPackage.SaveAs(new FileInfo(outputpath));
                }
                retVal = outputpath;
            }
            catch (Exception ex)
            {
                retVal = ex.Message;
                throw ex;
            }

            return retVal;
        }
    }

}